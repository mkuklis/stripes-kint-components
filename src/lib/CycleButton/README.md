# CycleButton
A component designed to render an icon button which cycles through a list of available icons, styles and actions.

## Basic Usage
```
import { CycleButton } from '@k-int/stripes-kint-components'

<CycleButton
  buttons={[
    {
      icon: "comment",
      onClick: () => console.log("CLICKED BUTTON"),
    },
    {
      icon: "trash",
      onClick: () => console.log("CLICKED OTHER BUTTON"),
    }
  ]}
/>
```


## Props

Name | Type | Description | default | required
--- | --- | --- | --- | ---
buttons | array\<Object> | An array of objects corresponding to states in the cycle of the action button. Object of the form `{icon: "trash", onClick: () => alert("Clicked trash icon"), ...rest }`, where `icon` and `onClick` are mandatory props and all other props are directly passed through to the underlying `IconButton`. On clicking the icon button, the `onClick` call will fire before the button is moved onto the next state. | | ✓ |
startIndex | integer | The starting index in the array for the cycle. | 0 | ✕ |