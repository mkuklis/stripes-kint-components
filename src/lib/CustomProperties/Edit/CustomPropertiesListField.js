import { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Field, useFormState } from 'react-final-form';

import { Button, Headline, KeyValue } from '@folio/stripes/components';
import { useKintIntl } from '../../hooks';
import CustomPropertyFormCard from './CustomPropertyFormCard';

const CustomPropertiesList = ({
  availableCustomProperties = [],
  input: {
    name,
    onChange,
    value
  },
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  meta: {
    pristine
  },
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const [customProperties, setCustomProperties] = useState([]); // This is the list of customProperties we're currently displaying for edit.
  const [dirtying, setDirtying] = useState(false);

  useEffect(() => {
    // When the user loads this form, we want to init the list of customProperties
    // we're displaying (state.customProperties) with the list of customProperties that have been set
    // either via defaults or previously-saved data. Since that data may come in
    // _after_ we have mounted this component, we need to check if new data has come in
    // while the form is still marked as pristine.
    //
    // final-form unsets `pristine` after its `onChange` is called, but we also dirty
    // the component when we add/remove rows. That happens _before_ `onChange` is called,
    // so internally we use `state.dirtying` to show that we just initiated an action
    // that will result in a dirty component.
    if (pristine && !dirtying) {
      setCustomProperties(availableCustomProperties.filter(
        customProperty => value[customProperty.value] !== undefined
      ));
    }
  }, [availableCustomProperties, dirtying, pristine, value]);

  const handleDeleteCustomProperty = (customProperty, i) => {
    const currentValue = value[customProperty.value]?.[0] ?? {};

    const newCustomProperties = [...customProperties];
    newCustomProperties.splice(i, 1);
    setCustomProperties(newCustomProperties);
    setDirtying(true);

    onChange({
      ...value,
      [customProperty.value]: [{
        ...currentValue,
        _delete: true,
      }],
    });
  };

  const renderCustomProperties = (customPropertyType) => {
    // This is necessary to track individually since "index" will span primary/optional for a given set
    let internalPropertyCounter = 0;
    return customProperties.map((customProperty, index) => {
      if (customPropertyType === 'primary' && !customProperty.primary) return undefined;
      if (customPropertyType === 'optional' && customProperty.primary) return undefined;

      internalPropertyCounter += 1;

      return (
        <CustomPropertyFormCard
          key={`customPropertyField-${customProperty.value}-${index}`}
          {...{
            availableCustomProperties,
            customProperty,
            customPropertyType,
            customProperties,
            handleDeleteCustomProperty,
            index,
            internalPropertyCounter,
            intlKey: passedIntlKey,
            intlNS: passedIntlNS,
            labelOverrides,
            name,
            onChange,
            setCustomProperties,
            value
          }}
        />
      );
    }).filter(cp => cp !== undefined);
  };

  return (
    <>
      {
        availableCustomProperties.some((customProperty = {}) => customProperty.primary) &&
          <KeyValue
            label={
              <Headline margin="x-small" size="large" tag="h4">
                {kintIntl.formatKintMessage({
                  id: 'customProperties.primaryProperties',
                  overrideValue: labelOverrides.primaryProperties
                })}
              </Headline>
            }
            value={renderCustomProperties('primary')}
          />
      }
      {
        availableCustomProperties.some((customProperty = {}) => !customProperty.primary) &&
          <KeyValue
            label={
              <Headline margin="x-small" size="large" tag="h4">
                {kintIntl.formatKintMessage({
                  id: 'customProperties.optionalProperties',
                  overrideValue: labelOverrides.optionalProperties
                })}
              </Headline>
            }
            value={renderCustomProperties('optional')}
          />
      }
      {
        availableCustomProperties.some((customProperty = {}) => !customProperty.primary) &&
        <Button
          id="add-customproperty-btn"
          onClick={() => {
            setCustomProperties([...customProperties, {}]);
            setDirtying(true);
          }}
        >
          {kintIntl.formatKintMessage({
            id: 'customProperties.addProperty',
            overrideValue: labelOverrides.addProperty
          })}
        </Button>
      }
    </>
  );
};

const CustomPropertiesListField = ({
  ctx,
  customProperties,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  ...fieldProps
}) => {
  const fieldRef = useRef();
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  // Map customProperties to bring together the options and the definition values
  const availableCustomProperties = customProperties?.map(customProperty => {
    let options = customProperty?.category?.values;
    if (options) {
      options = [
        {
          label: kintIntl.formatKintMessage({
            id: 'notSet',
            overrideValue: labelOverrides.notSet
          }),
          value: '',
        },
        ...options,
      ];
    }

    return {
      description: customProperty.description,
      label: customProperty.label,
      primary: customProperty.primary,
      type: customProperty.type,
      options,
      value: customProperty.name,
      defaultInternal: customProperty.defaultInternal,
    };
  });

  const { initialValues } = useFormState();
  const getInitialValue = () => {
    const cps = {};
    (customProperties || [])
      .filter(cp => cp.primary)
      // Change default to be an ignored customProperty.
      // This means any changes without setting the value will be ignored
      .forEach(cp => { cps[cp.name] = [{ _delete: true }]; });

    // IMPORTANT -- All customproperty ctx sections are adding to the same "initialValue" field
    // Ensure that we don't already have initialValues for this particular set before setting them,
    // to ensure no looping behaviour
    if (Object.keys(cps).every(key => initialValues.customProperties?.[key] !== undefined)) {
      return initialValues.customProperties;
    }

    // Ensure that if we already had these values in initialvalues they're not overwritten
    return ({ ...cps, ...initialValues.customProperties });
  };

  return (
    <Field
      {...fieldProps}
      initialValue={getInitialValue()}
      render={p => {
        return (
          <CustomPropertiesList
            ref={fieldRef}
            availableCustomProperties={availableCustomProperties}
            ctx={ctx}
            intlKey={passedIntlKey}
            intlNS={passedIntlNS}
            labelOverrides={labelOverrides}
            {...p}
          />
        );
      }}
    />
  );
};

CustomPropertiesListField.propTypes = {
  ctx: PropTypes.string,
  customProperties: PropTypes.arrayOf(PropTypes.object),
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
};

CustomPropertiesList.propTypes = {
  availableCustomProperties: PropTypes.arrayOf(PropTypes.object),
  ctx: PropTypes.string,
  input: PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    onChange: PropTypes.func,
  }),
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  meta: PropTypes.object,
};


export default CustomPropertiesListField;
