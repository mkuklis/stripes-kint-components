import PropTypes from 'prop-types';
import CustomPropertiesEditCtx from './CustomPropertiesEditCtx';

const CustomPropertiesEdit = ({
  contexts = [],
  customPropertiesEndpoint,
  id,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  nameOverride
}) => {
  return (
    contexts.map(ctx => (
      <CustomPropertiesEditCtx
        key={`customPropertiesEdit-${ctx}`}
        {...{
          ctx,
          customPropertiesEndpoint,
          id,
          intlKey: passedIntlKey,
          intlNS: passedIntlNS,
          labelOverrides,
          nameOverride
        }}
      />
    ))
  );
};

CustomPropertiesEdit.propTypes = {
  contexts: PropTypes.arrayOf(PropTypes.string),
  customPropertiesEndpoint: PropTypes.string,
  id: PropTypes.string,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  nameOverride: PropTypes.string
};

export default CustomPropertiesEdit;
