import PropTypes from 'prop-types';
import { Accordion, Badge, Spinner } from '@folio/stripes/components';

import { useCustomProperties, useKintIntl } from '../../hooks';

import CustomPropertyCard from './CustomPropertyCard';

const CustomPropertiesViewCtx = ({
  ctx,
  customProperties = [],
  customPropertiesEndpoint,
  id,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {}
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  // Deal with all the possible label override options
  const getAccordionLabel = () => {
    // Special case for null context
    if (ctx === 'isNull') {
      return (
        kintIntl.formatKintMessage({
          id: 'customProperties',
          overrideValue: labelOverrides.noContext
        })
      );
    }

    // Chain formatKintMessages together using provided fallbackMessage to
    // allow for "If override or translation exists, use it, else use default"
    return (
      kintIntl.formatKintMessage({
        id: `customProperties.ctx.${ctx}`,
        overrideValue: labelOverrides[ctx],
        fallbackMessage: kintIntl.formatKintMessage({
          id: 'customProperties.defaultTitle',
          overrideValue: labelOverrides.defaultTitle
        }, { ctx })
      }, { ctx })
    );
  };

  const { data: custprops, isLoading } = useCustomProperties({
    ctx,
    endpoint: customPropertiesEndpoint,
    returnQueryObject: true,
    options: {
      sort: [
        { path: 'retired' }, // Place retired custprops at the end
        { path: 'primary', direction: 'desc' }, // Primary properties should display before optional
        { path: 'weight' }, // Within those groups, sort by weight
        { path: 'label' } // For those with the same weight, sort by label
      ]
    }
  });

  if (isLoading) {
    return (
      <Accordion
        closedByDefault
        displayWhenClosed={<Spinner />}
        displayWhenOpen={<Spinner />}
        id={`${id}-accordion-${ctx}`}
        label={getAccordionLabel()}
      >
        <Spinner />
      </Accordion>
    );
  }

  const countSetProperties = (primary) => (
    Object.entries(customProperties)?.filter(
      ([_key, cp]) => (
        (cp[0]?.type?.ctx === ctx || (ctx === 'isNull' && !cp[0]?.type?.ctx)) && // Either the ctx string matches, or isNull matches blank
        cp[0]?.type?.primary === primary // Only count non-primary set custprops
      )
    )?.length ?? 0
  );

  // We need to display any set properties, along with any non-set primary properties
  const primaryCount = custprops?.filter(cp => cp.primary === true)?.length ?? 0;
  const optionalCount = countSetProperties(false);
  const setPrimaryCount = countSetProperties(true);

  return (
    (primaryCount + optionalCount) > 0 &&
    <Accordion
      closedByDefault
      displayWhenClosed={<Badge>{setPrimaryCount + optionalCount}</Badge>}
      displayWhenOpen={<Badge>{setPrimaryCount + optionalCount}</Badge>}
      id={`${id}-accordion-${ctx}`}
      label={getAccordionLabel()}
    >
      {custprops.map((cp, index) => (
        <CustomPropertyCard
          key={`custom-property-card-${ctx}[${index}]`}
          ctx={ctx}
          customProperty={customProperties?.[cp.name]?.[0]}
          customPropertyDefinition={cp}
          index={index}
          labelOverrides={labelOverrides}
        />
      ))}
    </Accordion>
  );
};

CustomPropertiesViewCtx.propTypes = {
  ctx: PropTypes.string,
  customProperties: PropTypes.object,
  customPropertiesEndpoint: PropTypes.string,
  id: PropTypes.string,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
};

export default CustomPropertiesViewCtx;
