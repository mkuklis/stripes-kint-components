import React from 'react';

import { MemoryRouter } from 'react-router-dom';
import { Select } from '@folio/stripes-testing';
import { TestForm } from '@folio/stripes-erm-testing';

import CustomPropertiesRule from './CustomPropertiesRule';
import { renderWithKintHarness } from '../../../../test/jest';

const onSubmit = jest.fn();
const onDelete = jest.fn();

jest.mock('../../hooks');

const clearRuleValue = () => {};

describe('CustomPropertiesRule', () => {
  beforeEach(() => {
     renderWithKintHarness(
       <MemoryRouter>
         <TestForm onSubmit={onSubmit}>
           <CustomPropertiesRule
             ariaLabelledby="selected-custprop-name-0"
             clearRuleValue={clearRuleValue}
             index={0}
             name="filters[0].rules[0]"
             onDelete={onDelete}
           />
         </TestForm>
       </MemoryRouter>
    );
  });

  describe('select Is set operator from comparator dropdown', () => {
    beforeEach(async () => {
      await Select().choose('Is set');
    });

    it('renders expected Is set operator', async () => {
      await Select().has({ value: ' isSet' });
    });
  });

  describe('select Is not set operator from comparator dropdown', () => {
    beforeEach(async () => {
      await Select().choose('Is not set');
    });

    it('renders expected is not set operator in comparator dropdown', async () => {
      await Select().has({ value: ' isNotSet' });
    });
  });

  describe('select Equals from comparator dropdown', () => {
    beforeEach(async () => {
      await Select().choose('Equals');
    });

    it('renders expected Equals operator', async () => {
      await Select().has({ value: '==' });
    });
  });

  describe('select not equal operator from comparator dropdown', () => {
    beforeEach(async () => {
      await Select().choose('Does not equal');
    });

    it('renders expected Does not equal operator in comparator dropdown', async () => {
      await Select().has({ value: '!=' });
    });
  });

  describe('select Is greater than or equal to operator from comparator dropdown', () => {
    beforeEach(async () => {
      await Select().choose('Is greater than or equal to');
    });

    it('renders expected Is greater than or equal to operator in comparator dropdown', async () => {
      await Select().has({ value: '>=' });
    });
  });

  describe('select Is less than or equal to operator from comparator dropdown', () => {
    beforeEach(async () => {
      await Select().choose('Is less than or equal to');
    });

    it('renders expected Is less than or equal to operator in comparator dropdown', async () => {
      await Select().has({ value: '<=' });
    });
  });

  describe('select Is operator from comparator dropdown', () => {
    beforeEach(async () => {
      await Select().choose('Is');
    });

    it('renders expected is operator', async () => {
      await Select().has({ value: '==' });
    });
  });

  describe('select Is not operator form comparator dropdown', () => {
    beforeEach(async () => {
      await Select().choose('Is not');
    });

    it('renders expected is not operator ', async () => {
      await Select().has({ value: '!=' });
    });
  });
});
