import React from 'react';

import { Button } from '@folio/stripes-testing';
import CustomPropertyFiltersForm from './CustomPropertiesFilterForm';

import { data } from './testResources';
import { renderWithKintHarness } from '../../../../test/jest';

jest.mock('../../hooks');
jest.mock('./CustomPropertiesFilterFieldArray', () => () => <div>CustomPropertiesFilterFieldArray</div>);

const onSubmit = jest.fn();

describe('CustomPropertyFiltersForm', () => {
  let renderComponent;
  beforeEach(() => {
    renderComponent = renderWithKintHarness(
      <CustomPropertyFiltersForm
        onSubmit={onSubmit}
        {...data}
      />
    );
  });

  it('renders CustomPropertiesFilterFieldArray component ', () => {
    const { getByText } = renderComponent;
    expect(getByText('CustomPropertiesFilterFieldArray')).toBeInTheDocument();
  });

  test('renders the apply button (disabled)', async () => {
    await Button('Apply').has({ disabled: true });
  });

  test('renders the Edit custom property filters button', async () => {
    await Button('Edit custom property filters').exists();
  });
});
