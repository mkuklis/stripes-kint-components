import React from 'react';

import { MemoryRouter } from 'react-router-dom';
import { KeyValue } from '@folio/stripes-testing';

import CustomPropertyView from './CustomPropertyView';
import customProperties from '../../../../test/jest/customProperties';
import { renderWithKintHarness } from '../../../../test/jest';

jest.mock('../../hooks');

describe('CustomPropertyView', () => {
  beforeEach(() => {
    renderWithKintHarness(
      <MemoryRouter>
        <CustomPropertyView
          customProperty={customProperties?.find(cp => cp.name === 'Eligible authors')}
        />
      </MemoryRouter>
    );
  });
  test('displays expected label value', async () => {
    await KeyValue('Label').has({ value: 'Does this agreement support publishing' });
  });

  test('displays expected name value', async () => {
    await KeyValue('Name').has({ value: 'Eligible authors' });
  });

  test('displays expected Description value', async () => {
    await KeyValue('Description').has({ value: 'Does this agreement support publishing' });
  });

  test('displays expected Primary value', async () => {
    await KeyValue('Primary').has({ value: 'Yes' });
  });

  test('displays expected Retired value', async () => {
    await KeyValue('Retired').has({ value: 'No' });
  });

  test('displays expected Weight value', async () => {
    await KeyValue('Order weight').has({ value: '0' });
  });

  test('displays expected Default visibility value', async () => {
    await KeyValue('Default visibility').has({ value: 'Internal' });
  });

  test('displays expected Context value', async () => {
    await KeyValue('Context').has({ value: 'OpenAccess' });
  });

  test('displays expected Type value', async () => {
    await KeyValue('Type').has({ value: 'Text' });
  });
});
