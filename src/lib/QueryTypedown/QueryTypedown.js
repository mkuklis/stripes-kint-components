import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Typedown from '../Typedown';
import { useTypedownData } from '../hooks/typedownHooks';

const QueryTypedown = ({
  dataFormatter = d => d,
  path,
  pathMutator,
  ...rest
}) => {
  const [callPath, setCallPath] = useState(pathMutator(null, path));
  const data = dataFormatter(useTypedownData(path, callPath));

  const onType = e => {
    setCallPath(pathMutator(e.target.value, path));
  };

  return (
    <Typedown
      {...rest}
      dataOptions={data}
      onType={onType}
    />
  );
};

QueryTypedown.propTypes = {
  dataFormatter: PropTypes.func,
  path: PropTypes.string,
  pathMutator: PropTypes.func
};

export default QueryTypedown;
