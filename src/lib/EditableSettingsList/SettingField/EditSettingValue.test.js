import React from 'react';


import { TestForm } from '@folio/stripes-erm-testing';

import EditSettingValue from './EditSettingValue';

import { renderWithKintHarness } from '../../../../test/jest/helpers';

const onSubmit = jest.fn();
jest.mock('../../hooks');

const stringSetting = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'String',
  value: 'diku-shared',
};

const refdata = [
  {
    id: '123',
    value: 'test_refdata_value',
    label: 'Test refdata value'
  },
  {
    id: '456',
    value: 'other_refdata_value',
    label: 'Other refdata value'
  },
  {
    id: '789',
    value: 'final_refdata_value',
    label: 'Final refdata value'
  }
];

const moreRefdata = [
  {
    id: '123',
    value: 'test_refdata_value',
    label: 'Test refdata value'
  },
  {
    id: '234',
    value: 'other_refdata_value',
    label: 'Other refdata value'
  },
  {
    id: '345',
    value: 'refdata_value_3',
    label: 'Refdata value 3'
  },
  {
    id: '456',
    value: 'refdata_value_4',
    label: 'Refdata value 4'
  },
  {
    id: '567',
    value: 'refdata_value_5',
    label: 'Refdata value 5'
  },
  {
    id: '678',
    value: 'final_refdata_value',
    label: 'Final refdata value'
  }
];

const refdataSetting = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'Refdata',
  value: 'test_refdata_value',
};

const templates = [
  {
    id: 'abcde',
    name: 'Test template'
  },
  {
    id: '98765',
    name: 'Red herring template'
  },
];

const templateSetting = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'Template',
  value: 'abcde',
};

describe('EditSettingValue', () => {
  describe('edit string setting', () => {
    describe('with no initial value', () => {
      let renderComponent;
      beforeEach(() => {
        renderComponent = renderWithKintHarness(
          <TestForm
            initialValues={{}}
            onSubmit={onSubmit}
          >
            <EditSettingValue
              currentSetting={stringSetting}
              input={{
                name: 'test'
              }}
            />
          </TestForm>
        );
      });
      test('renders the edit field', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('textbox', { name: 'Value for setting testSettingKey' }));
      });
    });

    describe('with initial value', () => {
      let renderComponent;
      beforeEach(() => {
        renderComponent = renderWithKintHarness(
          <TestForm
            initialValues={{
              test: {
                value: stringSetting.value
              }
            }}
            onSubmit={onSubmit}
          >
            <EditSettingValue
              currentSetting={stringSetting}
              input={{
                name: 'test'
              }}
            />
          </TestForm>
        );
      });

      test('renders the expected value in the edit field', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('textbox', { name: 'Value for setting testSettingKey' })).toHaveDisplayValue('diku-shared');
      });
    });
  });

  // NOTE -- Password textbox works weirdly so isn't tested here


  describe('edit refdata setting', () => {
    describe('with no initial value for refdata with <4 options', () => {
      let renderComponent;
      beforeEach(() => {
        renderComponent = renderWithKintHarness(
          <TestForm
            initialValues={{}}
            onSubmit={onSubmit}
          >
            <EditSettingValue
              currentSetting={refdataSetting}
              input={{
                name: 'test'
              }}
              refdata={refdata}
            />
          </TestForm>
        );
      });
      test('renders the expected radio buttons', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('radio', { name: 'Test refdata value' }));
        expect(getByRole('radio', { name: 'Other refdata value' }));
        expect(getByRole('radio', { name: 'Final refdata value' }));
      });

      test('no radio buttons are checked', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('radio', { name: 'Test refdata value' })).not.toHaveAttribute('checked');
        expect(getByRole('radio', { name: 'Other refdata value' })).not.toHaveAttribute('checked');
        expect(getByRole('radio', { name: 'Final refdata value' })).not.toHaveAttribute('checked');
      });
    });

    describe('with initial value for refdata with <4 options', () => {
      let renderComponent;
      beforeEach(() => {
        renderComponent = renderWithKintHarness(
          <TestForm
            initialValues={{
              test: {
                value: refdataSetting.value
              }
            }}
            onSubmit={onSubmit}
          >
            <EditSettingValue
              currentSetting={refdataSetting}
              input={{
                name: 'test'
              }}
              refdata={refdata}
            />
          </TestForm>
        );
      });

      test('renders the expected radio buttons', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('radio', { name: 'Test refdata value' }));
        expect(getByRole('radio', { name: 'Other refdata value' }));
        expect(getByRole('radio', { name: 'Final refdata value' }));
      });

      test('the correct radio button is checked', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('radio', { name: 'Test refdata value' })).toHaveAttribute('checked');
        expect(getByRole('radio', { name: 'Other refdata value' })).not.toHaveAttribute('checked');
        expect(getByRole('radio', { name: 'Final refdata value' })).not.toHaveAttribute('checked');
      });
    });

    describe('with no initial value for refdata with >4 options', () => {
      let renderComponent;
      beforeEach(() => {
        renderComponent = renderWithKintHarness(
          <TestForm
            initialValues={{}}
            onSubmit={onSubmit}
          >
            <EditSettingValue
              currentSetting={refdataSetting}
              input={{
                name: 'test'
              }}
              refdata={moreRefdata}
            />
          </TestForm>
        );
      });
      test('renders the expected select field', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('combobox', { name: 'Value for setting testSettingKey' }));
      });

      test('renders the expected refdata options', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('option', { name: 'Test refdata value' }));
        expect(getByRole('option', { name: 'Other refdata value' }));
        expect(getByRole('option', { name: 'Refdata value 3' }));
        expect(getByRole('option', { name: 'Refdata value 4' }));
        expect(getByRole('option', { name: 'Refdata value 5' }));
        expect(getByRole('option', { name: 'Final refdata value' }));
      });
    });

    describe('with initial value for refdata with >4 options', () => {
      let renderComponent;
      beforeEach(() => {
        renderComponent = renderWithKintHarness(
          <TestForm
            initialValues={{
              test: {
                value: refdataSetting.value
              }
            }}
            onSubmit={onSubmit}
          >
            <EditSettingValue
              currentSetting={refdataSetting}
              input={{
                name: 'test'
              }}
              refdata={moreRefdata}
            />
          </TestForm>
        );
      });

      test('renders the expected refdata options', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('option', { name: 'Test refdata value' }));
        expect(getByRole('option', { name: 'Other refdata value' }));
        expect(getByRole('option', { name: 'Refdata value 3' }));
        expect(getByRole('option', { name: 'Refdata value 4' }));
        expect(getByRole('option', { name: 'Refdata value 5' }));
        expect(getByRole('option', { name: 'Final refdata value' }));
      });

      test('renders the expected value in the refdata dropdown', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('combobox', { name: 'Value for setting testSettingKey' })).toHaveDisplayValue('Test refdata value');
      });
    });
  });

  describe('edit template setting', () => {
    describe('with no initial value', () => {
      let renderComponent;
      beforeEach(() => {
        renderComponent = renderWithKintHarness(
          <TestForm
            initialValues={{}}
            onSubmit={onSubmit}
          >
            <EditSettingValue
              currentSetting={templateSetting}
              input={{
                name: 'test'
              }}
              templates={templates}
            />
          </TestForm>
        );
      });
      test('renders the edit field', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('combobox', { name: 'Value for setting testSettingKey' }));
      });

      test('renders the expected template options', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('option', { name: '' })); // empty option
        expect(getByRole('option', { name: 'Test template' }));
        expect(getByRole('option', { name: 'Red herring template' }));
      });

      test('renders expected display item in template dropdown', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('combobox', { name: 'Value for setting testSettingKey' })).toHaveDisplayValue('');
      });
    });

    describe('with initial value', () => {
      let renderComponent;
      beforeEach(() => {
        renderComponent = renderWithKintHarness(
          <TestForm
            initialValues={{
              test: {
                value: templateSetting.value
              }
            }}
            onSubmit={onSubmit}
          >
            <EditSettingValue
              currentSetting={templateSetting}
              input={{
                name: 'test'
              }}
              templates={templates}
            />
          </TestForm>
        );
      });

      test('renders the edit field', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('combobox', { name: 'Value for setting testSettingKey' }));
      });

      test('renders the expected template options', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('option', { name: '' })); // empty option
        expect(getByRole('option', { name: 'Test template' }));
        expect(getByRole('option', { name: 'Red herring template' }));
      });

      test('renders expected display item in template dropdown', () => {
        const { getByRole } = renderComponent;
        expect(getByRole('combobox', { name: 'Value for setting testSettingKey' })).toHaveDisplayValue('Test template');
      });
    });
  });
});
