import React from 'react';
import PropTypes from 'prop-types';

import { useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';

import { Settings } from '@folio/stripes/smart-components';

import { SettingPage, SettingPagePane } from '../SettingPage';
import { SettingsContext } from '../contexts';

import { generateKiwtQueryParams, sortByLabel, toCamelCase } from '../utils';
import { useKintIntl, useIntlKey } from '../hooks';

const useSettings = ({
  allowGlobalEdit = true,
  dynamicPageExclusions,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  persistentPages,
  refdataEndpoint,
  settingEndpoint,
  templateEndpoint
}) => {
  const ky = useOkapiKy();
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const queryParams = generateKiwtQueryParams({
      filters: dynamicPageExclusions?.map(dpe => ({
        path: 'section',
        comparator: '!=',
        value: dpe
      })),
      perPage: 100,
      stats: false
  }, {});

  const { data: appSettings = [], isLoading } = useQuery(
    ['stripes-kint-components', 'useSettings', 'appSettings', settingEndpoint, queryParams],
    () => ky(`${settingEndpoint}?${queryParams?.join('&')}`).json()
  );

  const sections = [...new Set(appSettings.map(s => s.section))];
  const dynamic = sections.map(section => {
    return (
      {
        route: section,
        label: kintIntl.formatKintMessage({
          id: `settings.settingsSection.${toCamelCase(section)}`,
          fallbackMessage: section
        }),
        component: (props) => (
          <SettingPagePane
            intlKey={passedIntlKey}
            intlNS={passedIntlNS}
            sectionName={section}
          >
            <SettingPage
              allowEdit={allowGlobalEdit}
              intlKey={passedIntlKey}
              intlNS={passedIntlNS}
              labelOverrides={labelOverrides}
              sectionName={section}
              {...props}
            />
          </SettingPagePane>
        )
      }
    );
  });

  const pageList = persistentPages.concat(dynamic).sort(sortByLabel);

  const intlKey = useIntlKey(passedIntlKey, passedIntlNS);
  const SettingsContextProvider = ({ children }) => {
    return (
      <SettingsContext.Provider
        value={{
          intlKey, // This is only here for backwards compatibility... is now grabbed from useIntlKey instead of what's passed in directly
          refdataEndpoint,
          settingEndpoint,
          templateEndpoint
        }}
      >
        {children}
      </SettingsContext.Provider>
    );
  };

  SettingsContextProvider.propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.node
    ])
  };

  const SettingsComponent = (props) => {
    return (
      <Settings
        pages={pageList}
        paneTitle={
          kintIntl.formatKintMessage({
          id: 'meta.title'
        })}
        {...props}
      />
    );
  };

  const SettingsComponentWithContext = (props) => {
    return (
      <SettingsContextProvider>
        <SettingsComponent {...props} />
      </SettingsContextProvider>
    );
  };

  return {
    isLoading,
    pageList,
    SettingsComponent: SettingsComponentWithContext,
    SettingsContextProvider
  };
};

export default useSettings;
