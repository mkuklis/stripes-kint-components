const groupCustomPropertiesByCtx = (customProperties = []) => (
  customProperties.reduce((acc, curr) => {
    const ctx = curr.ctx ?? 'isNull';
    const returnObj = {
      ...acc
    };

    returnObj[ctx] = [...(acc?.[ctx] ?? []), curr];
    return returnObj;
  }, {})
);

export default groupCustomPropertiesByCtx;
