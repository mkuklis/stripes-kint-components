const parseErrorResponse = async (responseObj) => {
  let errorResp;
  const code = responseObj?.status;
  const contentType = [...responseObj?.headers]?.find(header => header[0] === 'content-type')?.[1];

  if (contentType.includes('json')) {
    errorResp = await responseObj.json();
  } else {
    try {
      errorResp = { code, message: await responseObj.text() };
    } catch {
      errorResp = { code, message: 'something went wrong' };
    }
  }

  return errorResp;
};

export default parseErrorResponse;
