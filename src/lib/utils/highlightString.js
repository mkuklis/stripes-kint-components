import matchString from './matchString';

const highlightString = (match, str, ignoreNull = true) => {
  const [parts, regex] = matchString(match, str, ignoreNull);

  return (
    parts.filter(part => part).map((part, i) => (
      regex.test(part) ?
        <mark
          key={i}
        >
          {part}
        </mark> :
        <span key={i}>
          {part}
        </span>
    ))
  );
};

const boldString = (match, str, ignoreNull = true) => {
  const [parts, regex] = matchString(match, str, ignoreNull);

  return (
    parts.filter(part => part).map((part, i) => (
      regex.test(part) ?
        <strong
          key={i}
        >
          {part}
        </strong> :
        <span key={i}>
          {part}
        </span>
    ))
  );
};

export {
  boldString,
  highlightString
};
