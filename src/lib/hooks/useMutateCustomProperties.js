import { useMutation } from 'react-query';

import { useOkapiKy } from '@folio/stripes/core';

const useMutateCustomProperties = ({
  afterQueryCalls,
  catchQueryCalls,
  endpoint,
  id,
  queryParams,
  returnQueryObject = {
    post: false,
    put: false,
    delete: false
  }
}) => {
  const returnObj = {};

  const ky = useOkapiKy();

  const deleteQueryObject = useMutation(
    ['stripes-kint-components', 'useMutateCustomProperties', 'delete', id],
    async () => ky.delete(`${endpoint}/${id}`).json()
      .then(afterQueryCalls?.delete)
      .catch(catchQueryCalls?.delete),
    queryParams?.delete
  );

  const putQueryObject = useMutation(
    ['stripes-kint-components', 'useMutateCustomProperties', 'put', id],
    async (data) => ky.put(`${endpoint}/${id}`, { json: data }).json()
      .then(afterQueryCalls?.put)
      .catch(catchQueryCalls?.put),
    queryParams?.put
  );

  const postQueryObject = useMutation(
    ['stripes-kint-components', 'useMutateCustomProperties', 'post'],
    async (data) => ky.post(`${endpoint}`, { json: data }).json()
      .then(afterQueryCalls?.post)
      .catch(catchQueryCalls?.post),
    queryParams?.post
  );

  if (returnQueryObject?.delete) {
    returnObj.delete = deleteQueryObject;
  } else {
    returnObj.delete = deleteQueryObject.mutateAsync;
  }

  if (returnQueryObject?.put) {
    returnObj.put = putQueryObject;
  } else {
    returnObj.put = putQueryObject.mutateAsync;
  }

  if (returnQueryObject?.post) {
    returnObj.post = postQueryObject;
  } else {
    returnObj.post = postQueryObject.mutateAsync;
  }

  return returnObj;
};

export default useMutateCustomProperties;
