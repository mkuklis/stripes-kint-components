import refdata from '../../../../test/jest/refdata';
import customProperties from '../../../../test/jest/customProperties';

import useKintIntl from '../useKintIntl';

// We have to do this up here too so that our passed useKintIntl
// ALSO has a mocked useIntlKeyStore... I think anyway
jest.mock('../useIntlKeyStore', () => () => () => 'ui-test-implementor');

module.exports = {
  useRefdata: () => refdata,
  useTemplates: () => [], // We should set up some templates to test this properly
  useCustomProperties: () => ({ data: customProperties, isLoading: false }),
  useMutateCustomProperties: () => [],
  useIntlKeyStore: () => () => () => 'ui-test-implementor',
  useKintIntl, // Use the "proper" useKintIntl but with a mocked intlKeyStore which returns the ui-test-implementor
};
