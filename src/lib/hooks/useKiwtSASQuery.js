import React, { useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';

import buildUrl from '../utils/buildUrl';
import useQindex from './useQIndex';

const locationQuerySetter = ({ location, history, nsValues }) => {
  const { pathname, search } = location;
  const url = buildUrl(location, nsValues);

  // Do not push to history if the url didn't change
  // https://issues.folio.org/browse/STSMACOM-637
  if (`${pathname}${search}` !== url) {
    history.push(url);
  }
};

const useKiwtSASQuery = () => {
  const history = useHistory();
  const location = useLocation();
  const [query, setQuery] = useState({});
  const queryGetter = () => query;

  const [qindex] = useQindex();
  // Ensure we update the query along with the querySetter
  if (query.qindex !== qindex) {
    setQuery({ ...query, qindex });
  }

  const querySetter = ({ nsValues }) => {
    setQuery({ ...query, ...nsValues, qindex });
    locationQuerySetter({ location, history, nsValues });
  };
  return { query, queryGetter, querySetter };
};

export default useKiwtSASQuery;
