// We can use hooks within zustand store functions without violating the laws of hooks
/* eslint-disable react-hooks/rules-of-hooks */
import { useNamespace } from '@folio/stripes/core';
import { create } from 'zustand';

const useIntlKeyStore = create((set, get) => ({
  intlKeys: {},
  addKey: (key, namespace) => set((state) => {
    let ns = namespace;
    const folioNS = useNamespace()?.[0];
    if (!ns) {
      ns = folioNS;
    }

    return { ...state, intlKeys: { [ns]: key, ...state.intlKeys } };
  }),
  removeKey: (namespace) => set((state) => {
    let ns = namespace;
    const folioNS = useNamespace()?.[0];
    if (!ns) {
      ns = folioNS;
    }

    const { [ns]: _valueToRemove, ...newKeys } = state.intlKeys;
    return { ...state, intlKeys: newKeys };
  }),
  getKey: (namespace) => {
    let ns = namespace;
    const folioNS = useNamespace()?.[0];
    if (!ns) {
      ns = folioNS;
    }

    return get().intlKeys?.[ns];
  }
}));

export default useIntlKeyStore;
