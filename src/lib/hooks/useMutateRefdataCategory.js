import { useMutation } from 'react-query';

import { useOkapiKy } from '@folio/stripes/core';

import useInvalidateRefdata from './useInvalidateRefdata';

const useMutateRefdataCategory = ({
  afterQueryCalls,
  catchQueryCalls,
  endpoint,
  queryParams,
  returnQueryObject = {
    post: false,
    delete: false
  }
}) => {
  const returnObj = {};

  const ky = useOkapiKy();
  const invalidateRefdata = useInvalidateRefdata();

  const deleteQueryObject = useMutation(
    ['stripes-kint-components', 'useMutateRefdataCategory', 'delete'],
    async (id) => ky.delete(
      `${endpoint}/${id}`
).json()
      .then(afterQueryCalls?.delete)
      .then(() => invalidateRefdata())
      .catch(catchQueryCalls?.delete),
    queryParams?.delete
  );

  const postQueryObject = useMutation(
    ['stripes-kint-components', 'useMutateRefdataCategory', 'post'],
    async (payload) => ky.post(endpoint,
      {
        json: {
          ...payload,
          values: []
        }
      }).json()
      .then(afterQueryCalls?.post)
      .then(() => invalidateRefdata())
      .catch(catchQueryCalls?.post),
    queryParams?.post
  );

  if (returnQueryObject?.delete) {
    returnObj.delete = deleteQueryObject;
  } else {
    returnObj.delete = deleteQueryObject.mutateAsync;
  }

  if (returnQueryObject?.post) {
    returnObj.post = postQueryObject;
  } else {
    returnObj.post = postQueryObject.mutateAsync;
  }

  return returnObj;
};

export default useMutateRefdataCategory;
