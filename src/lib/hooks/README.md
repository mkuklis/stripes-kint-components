# hooks
A collection of useful hooks

## useRefdata
A hook for fetching refdata values
### Basic usage
```
import { useRefdata } from '@k-int/stripes-kint-components'
...
const data = useRefdata({ 
  desc: 'PublicationRequest.PublicationType',
  endpoint: 'oa/refdata'
});
...
```

### Props
Name | Type | Description | default | required
--- | --- | --- | --- | ---
endpoint | string | The endpoint to fetch refdataValues from | | ✓ |
desc | string | The refdataCategory (usually of the form `DomainClass.Field`) | | ✕ |
queryParams | object | A set of queryParameters to hand to react-query's `useQuery` | | ✕ |
returnQueryObject | bool | A switch to return the entirety of the queryObject from useQuery. If `false`, the data will be destructured, if `true` the return will be the full object returned by react-query's `useQuery` | false | ✕ |
options | object | An object of the shape SASQ_MAP (See generateKiwtQuery) to pass to the generateKiwtQuery inside. Any passed desc "d" will be passed as a filter `DescKey.${d}`, with DescKey acting as FilterName, assuming `filterKeys: { DescKey: "desc" }` in options, so `desc==${d}` is passed directly to the backend. | `{filterKeys: {DescKey: "desc" }, stats: false, max: 100}` | ✕ |

## useMutateRefdataValue
A hook for mutations (Create/Delete/Edit) of refdataValues.

### Basic Usage
```
import { useMutateRefdataValue } from '@k-int/stripes-kint-components'
...
const { delete } = useMutateRefdataValue({
  endpoint: 'oa/refdata',
  id: '1234-abcd-5678',
});
```

### Props
Name | Type | Description | default | required
--- | --- | --- | --- | ---
afterQueryCalls | object | An object of the form `{delete: func1, put: func2}` where `func1`/`func2` are functions to be called after the `delete`/`put`. This is for ease of use, you can alternatively manually use a `.then()` on the functions returned from this hook. | | ✓ |
catchQueryCalls | object | An object of the form `{delete: func1, put: func2}` where `func1`/`func2` are functions to be called with a HTTPError object when the `delete`/`put` calls fail. This is for ease of use, you can alternatively manually use a `.catch()` on the functions returned from this hook. | | ✕ |
endpoint | string | The refdata endpoint | | ✕ |
id | string | The id of the refdata whose values are to be mutated | | ✕ |
queryParams | object | An object of the form `{delete: obj1, put: obj2}`, where `obj1`/`obj2` are set of queryParameters to hand to react-query's `useMutation` for the delete/put operations respectively | | ✕ |
returnQueryObject | object | An object of the form `{delete: bool1, put: bool2}` containing switches to return the entirety of the queryObject from useMutation for `delete` and `put` respectively. If `false`, the mutateAsync function will be destructured, if `true` the return will be the full object returned by react-query's `useMutation` | {put: false, delete: false} | ✕ |

### Example
```
...
const { delete, put } = useMutateRefdataValue({
  afterQueryCalls: {
    delete: json => setContentData(json?.values ?? []),
    put: json => setContentData(json?.values ?? [])
  },
  endpoint: 'oa/refdata',
  id: refdata?.id,
  queryParams: {
    delete: {
      enabled: !!refdata
    },
    put: {
      enabled: !!refdata
    }
  }
});
...
```

This block will destructure two `mutateAsync` functions, `delete` and `put`. After a call to either of them the `json` returned will be used in a function call `setContentData`.

The calls will only be enabled if `refdata` is truthy.

`put` will return a function `put(data)` and will make a `put` call to add the `data` to the values of the given refdata id. In practice this means calling `put` as follows:
```
put({
  value: 'testValue'
  label: 'testLabel'
})
```
for a new refdataValue, or
```
put({
  id: <id of the refdataValue to edit>
  value: 'testValue2'
  label: 'testLabel 2'
})
```
to edit an existing value.


`delete` will return a function `delete(data)` and will make a `put` call to glue `_delete: true` to the given value. In practice this means calling `delete` as follows:
```
delete(<ID of refdata value to remove>)
```

## useActiveElement
A hook which returns the currently focused element in the document, `active`, as well as a function `hasParent`.
The hooks works by creating a listener on the document for the `focusin` event and then setting state using `document.activeElement`.
### Basic usage
```
import { useActiveElement } from '@k-int/stripes-kint-components'
...
const { active, hasParent } = useActiveElement();
...
```

### hasParent
The function `hasParent` takes a single string as a prop, and returns `true` if and only if the currently active element is a child of some element with `id` attribute matching the regex `[id^=${id}]`.
Usage is as follows:
```
  const { hasParent } = useActiveElement();
  const openBool = hasParent('typedown-parent');
```

## useHelperApp
A hook which takes an object containing various helper components to render, and handles the url logic to decide which one to render.
When the url contains a query of the form `helper={name}`, this hook will check the helpers object it was handed for a key matching `{name}`, and if there is one it will return the component value for that key.

The hook also returns an object `helperToggleFunctions`, which will have the same keys as were handed to the hook, and values corresponding to "toggle" functions for that key. These functions are a simple way to change the helper query in the url.

Finally the hook also returns an ease-of-use function `isOpen`, which takes a string input and returns a boolean if the current helper in the URL matches that string or not.
### Basic usage
```
import { useHelperApp } from '@k-int/stripes-kint-components'
...
const ChatPane = () => { ... }
const Tags = () => { ... }
...
const { HelperComponent, helperToggleFunctions } = useHelperApp({
  chat: ChatPane,
  tags: Tags
});
...

return (
  <>
    <button onClick={helperToggleFunctions.chat}> Chat </button>
    <button onClick={helperToggleFunctions.tags}> Tags </button>
    < ... />
    <HelperComponent />
  </>
);
```

### Props
Name | Type | Description | default | required
--- | --- | --- | --- | ---
helpers | object | An object of the form `{helperKey1: Component1, helperKey2: Component1}` where `Component1`/`Component1` are components to be rendered when the url contains query `helper=helperKey1` or `helper=helperKey2` respectively | | ✓ |

## useKiwtSASQuery
A hook which sets up a basic queryGetter and querySetter for use with SASQ, as well as setting up the query object itself. Will often be used with the `generateKiwtQuery` function from `utils`.

### Basic usage
```
import { generateKiwtQuery, useKiwtSASQuery } from '@k-int/stripes-kint-components';
import { useOkapiKy } from '@folio/stripes/core';
import { useQuery } from 'react-query';

...
  const { query, queryGetter, querySetter } = useKiwtSASQuery();

  const SASQ_MAP = {
    searchKey: 'requestStatus.value',
    filterKeys: {
      requestStatus: 'requestStatus.value'
    }
  };

  const ky = useOkapiKy();

  const { data } = useQuery(
    ['ui-oa', 'oaRoute', 'publicationRequests', query],
    () => ky(`oa/publicationRequest${generateKiwtQuery(SASQ_MAP, query)}`).json()
  );
...
  return (
    <SearchAndSortQuery
      initialSearchState={{ query: '' }}
      queryGetter={queryGetter}
      querySetter={querySetter}
    >
    ...
    </SearchAndSortQuery>

```

## useQIndex
A hook with a similar API to setState, but instead stores a value in the `qindex` parameter in the URL.
Returns an array `[qindex, setQindex]`. There is no way to set an initialValue with the hook, as the state is derived from the URL rather than the other way around. This means that multiple `useQIndex` hooks can exist at once.

### BasicUsage
```
import {
  Button,
} from '@folio/stripes/components';
import useQindex from '../../../../stripes-kint-components/src/lib/hooks/useQIndex';

const Test() {
  const [qindex, setQindex] = useQindex();

  return (
    <>
      <Button
        onClick={() => setQindex(qindex + "1")}
      >
        change the qindex
      </Button>
      <div>
        {`The current qindex is: ${qindex}`
      </div>
    </>
  );
}

export default Test;
```

## useKiwtFieldArray
A hook to replace `withKiwtFieldArray` [from stripes-erm-components](https://github.com/folio-org/stripes-erm-components/tree/master/lib/withKiwtFieldArray). Provides functions for adding and deleting items in a way which the backend endpoints will understand, as well as an "items" array to track the current set of valid items in the form.


### BasicUsage
```
...
import useKiwtFieldArray from '../../../util/useKiwtFieldArray';
...

const TestFieldArray = () => {
  const {
    items,
    onAddField,
    onDeleteField
  } = useKiwtFieldArray(name, true);

  return (
    <>
      {items.map((item, index) => {
        return (
          <Row>
            <Col xs={3}>
              <Field
                name={`${name}[${index}].name`}
                component={TextField}
                required
                validate={requiredValidator}
              />
            </Col>
            <Col xs={1}>
              <IconButton
                icon="trash"
                id="remove-volume-button"
                onClick={() => onDeleteField(index, volume)}
              />
            </Col>
          </Row>
        );
      })}
      <Button
        onClick={() => onAddField({}})}
      >
        ADD NEW
      </Button>
    </>
  );
};

const Test = ({}) => {
  return (
    <Form
      ...
      render={({ handleSubmit, submitting, form }) => (
        <form onSubmit={handleSubmit}>
          <FieldArray
            name="test"
            component={TestFieldArray}
          />
        </form>
      )}
    />
  );
};
export default Test
```

### Props
Name | Type | Description | default | required
--- | --- | --- | --- | ---
name | String | The name of the fieldArray, used to hook into the final form state for that field. | | ✓ |
submitWholeDeletedObject | bool | a boolean flag to ensure that a deleted object is sent whole to the backend, rather than just as an id. | false | ✕ |

## useAppSettings
A hook for fetching AppSetting values
### Basic usage
```
import { useAppSettings } from '@k-int/stripes-kint-components'
...
const data = useAppSettings({ 
  endpoint: 'oa/settings/appSettings'
});
...
```

This will return a list of AppSettings. If a `keyName` is passed, this will instead return a single AppSetting object.

### Props
Name | Type | Description | default | required
--- | --- | --- | --- | ---
endpoint | string | The endpoint to fetch AppSettings from | | ✓ |
sectionName | string | A string representing a section of AppSettings to filter on | | ✕ |
keyName | string | A string representing an individual key of an AppSettings to filter on. The presence of this prop will change the output shape from Array to Object. It is not strictly necessary to use `keyName` in conjunction with `sectionName`, but keys are not guaranteed to be unique between sections. In addition it may marginally improve performance to use both, even if a key is unique, so it is recommended. | | ✕ |
queryParams | object | A set of queryParameters to hand to react-query's `useQuery` | | ✕ |
returnQueryObject | bool | A switch to return the entirety of the queryObject from useQuery. If `false`, the data will be destructured, if `true` the return will be the full object returned by react-query's `useQuery` | false | ✕ |
options | object | An object of the shape SASQ_MAP (See generateKiwtQuery) to pass to the generateKiwtQuery inside. The default | `{perPage: 100, stats: false, filters: [{path: 'section', value: sectionName }, {path: 'key', value: keyName }]}` | ✕ |
