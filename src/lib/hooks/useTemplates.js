import React from 'react';
import { useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';

const useTemplates = ({ context, endpoint, queryParams, sort }) => {
  const ky = useOkapiKy();

  const extraPathItems = [];
  if (context) {
    extraPathItems.push(`filters=context=${context}`);
  }
  if (sort) {
    extraPathItems.push(`sort=${sort}`);
  }
  extraPathItems.push('max=500');

  const path = `${endpoint}?${extraPathItems.join('&')}`;
  const { data: templates } = useQuery(
    ['stripes-kint-components', 'useTemplates', 'templates', context ?? ''],
    () => ky(path).json(),
    queryParams
  );

  return templates || [];
};

export default useTemplates;
