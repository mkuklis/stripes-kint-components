import { useCallback, useState } from 'react';

const useSelectedOption = () => {
  const [selectedOption, setSelectedOption] = useState();
  const ref = useCallback(node => {
    if (node !== null && selectedOption?.value !== node.selectedOption?.value) {
      setSelectedOption(node.selectedOption);
    }
  }, [selectedOption]);

  return ([ref, selectedOption]);
};

export default useSelectedOption;
