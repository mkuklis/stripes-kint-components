import { translationsProperties as coreTranslations } from '@folio/stripes-erm-testing';

import testTranslations from './test-implementor-translations';

const translationsProperties = [
  ...coreTranslations,
  {
    // Extra translations to allow us to properly test the translation pattern as if we had an implementing module
    prefix: 'ui-test-implementor',
    translations: testTranslations
  }
];

export default translationsProperties;
