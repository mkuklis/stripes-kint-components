import React from 'react';
import PropTypes from 'prop-types';
import { Harness } from '@folio/stripes-erm-testing';

import { SettingsContext } from '../../../src/lib/contexts';
import translationsProperties from '../../helpers';


export default function KintHarness({
  children,
  settingsValues = {
    intlKey: 'stripes-kint-components',
    refdataEndpoint: 'path/to/refdata',
    settingEndpoint: 'path/to/settings',
    templateEndpoint: 'path/to/templates'
  },
  translations = translationsProperties,
  ...harnessProps
}) {
  return (
    <SettingsContext.Provider value={settingsValues}>
      <Harness
        translations={translations}
        {...harnessProps}
      >
        {children}
      </Harness>
    </SettingsContext.Provider>
  );
}

KintHarness.propTypes = {
  children: PropTypes.node,
  settingsValues: PropTypes.object,
  translations: PropTypes.arrayOf(PropTypes.object)
};
