import React from 'react';
import { render } from '@testing-library/react';

import KintHarness from './KintHarness';
import translationsProperties from '../../helpers/translationsProperties';

const renderWithKintHarness = (children, context, translations = translationsProperties, renderer = render) => renderer(
  <KintHarness
    settingsValues={context}
    translations={translations}
  >
    {children}
  </KintHarness>
);

export default renderWithKintHarness;
